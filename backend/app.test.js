//app.test.js

const request = require('supertest')
const app = require('./app')
const mongoose = require("mongoose");

describe('Test endpoint POST /addData', () => {
    beforeEach(async () => {
        await mongoose.connect(`mongodb://${process.env.DB_HOST}:27017/mydatabase`);
    });
    
    afterEach(async () => {
        await mongoose.connection.close();
    });

    it('Simple test Visuel - OK', async () => {

        const response = await request(app)
            .post('/addData')
            .send({ name: 'Test Data' });

        expect(response.status).toBe(200);
        expect(response.body.message).toBe('Donnée ajoutée avec succès.');
    });

    it('Simple test Visuel - KO', async () => {
        const response = await request(app)
            .post('/addData')
            .send({});

        expect(response.status).toBe(400);
        expect(response.body.error).toBe('Le champ "name" est requis.');
    });
});