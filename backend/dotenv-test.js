const path = require('path');
const dotenv = require('dotenv');

let envFilepath = ""

if (process.env.CI) {
    envFilepath = ".env.ci"
} else {
    envFilepath = ".env.local"
}

module.exports = async () => {
    dotenv.config({ path: path.resolve(__dirname, envFilepath) });
};