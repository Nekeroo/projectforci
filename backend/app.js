// app.js

const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');


const app = express();
const port = 3000; // ou tout autre port de votre choix

// Middleware pour parser le corps des requêtes HTTP
app.use(bodyParser.json());

// Connexion à MongoDB
mongoose.connect(`mongodb://${process.env.DB_HOST}:27017/mydatabase`, { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;

db.on('error', console.error.bind(console, 'Erreur de connexion à MongoDB :'));
db.once('open', () => {
  console.log('Connecté à MongoDB');
});

// MongoDB modèle de schéma
const itemSchema = new mongoose.Schema({
  name: String,
});

const Item = mongoose.model('Item', itemSchema);

// Endpoint pour ajouter une donnée
/**
 * @swagger
 * /addData:
 *   post:
 *     summary: Ajouter une donnée
 *     description: Endpoint pour ajouter une donnée dans la base de données.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: Le nom de l'élément à ajouter.
 *     responses:
 *       200:
 *         description: Succès de l'opération.
 *       400:
 *         description: Mauvaise requête.
 *       500:
 *         description: Erreur interne du serveur.
 */
app.post('/addData', async (req, res) => {
  try {
    const itemName = req.body.name;

    if (!itemName) {
      return res.status(400).json({ error: 'Le champ "name" est requis.' });
    }

    const newItem = new Item({
      name: itemName,
    });

    await newItem.save();
    return res.status(200).json({ message: 'Donnée ajoutée avec succès.' });
  } catch (error) {
    console.error('Erreur lors de l\'ajout de la donnée :', error);
    return res.status(500).json({ error: 'Erreur lors de l\'ajout de la donnée.' });
  }
});

// Définition de la configuration Swagger
const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: 'API Documentation',
      version: '1.0.0',
      description: 'Documentation de l\'API avec Swagger',
    },
  },
  apis: ['./app.js'], // Chemin vers le fichier contenant les annotations Swagger
};

// Initialisation de Swagger JSDoc
const swaggerSpec = swaggerJSDoc(swaggerOptions);

// Endpoint pour servir la documentation Swagger
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// Export du module app pour les tests Jest
module.exports = app;

// Démarrer le serveur si le fichier est exécuté directement
if (require.main === module) {
  app.listen(port, () => {
    console.log(`Serveur en écoute sur le port ${port}`);
  });
}
